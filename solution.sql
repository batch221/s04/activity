/*
 Create an solution.sql file inside s04/a1 project and do the following using the music_db database:
 	a. Find all artist that has letter d in its name.
 	b. Find all songs that has a lenght of less than 230.
 	c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)
 	d. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
 	e. Sort the albums in Z-A order. (Show only the first 4 records.)
 	f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

*/
-- a. Find all artist that has letter d in its name.
SELECT * FROM songs WHERE name LIKE "%d%";

-- b. Find all songs that has a lenght of less than 230.
SELECT * FROM songs WHERE length < 230;

-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)
SELECT song_name, song_length FROM songs
    JOIN albums ON songs.album_id = albums.id;
	

-- d. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id;
    SELECT * FROM artists WHERE name LIKE "%a%";

-- e. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC;

-- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM albums 
    JOIN songs ON albums.id = songs.album_id;
    SELECT * FROM albums ORDER BY album_title DESC;